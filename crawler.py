#!/usr/bin/env python

import sys
import requests
import lxml.html
import bs4


def find_ttl_by_name(name):
    url = "http://m.imdb.com/find?ref_=nv_sr_fn&s=tt&q=" + name
    page = requests.get(url)

    soup = bs4.BeautifulSoup(page.content, "lxml")
    ttl = ""
    j = 0
    for s in soup.findAll('div'):
        if s.get('class') is not None and s.get('class')[0] == 'title':
            ttl = s.contents[1].get('href')
            break

    return ttl


def find_move_by_name(name):
    ttl = find_ttl_by_name(name.replace(" ", "+"))
    if len(ttl) > 0:
        ttl = ttl.split("/")[2]
        return find_detail_by_id(ttl)


def find_detail_by_id(id):
    hxs = lxml.html.document_fromstring(requests.get("http://imdb.com/title/" + id).content)
    movie = {'ttl': id}
    try:
        movie['title'] = hxs.xpath('//h1')[0].text_content().split("\xa0")[0]
    except IndexError:
        movie['title'] = ""
    try:
        movie['year'] = hxs.xpath('//h1')[0].text_content().split("\xa0")[1].replace(" ", "").replace("(", "").replace(
            ")", "")
    except IndexError:
        pass

    try:
        movie['genre'] = [i.text_content() for i in hxs.xpath(
            '//*[contains(concat( " ", @class, " " ), concat( " ", "subtext", " " ))]//*[contains(concat( " ", @class, " " )\
            , concat( " ", "itemprop", " " ))]')]
    except IndexError:
        movie['genre'] = []
    try:
        movie['rating'] = hxs.xpath('//strong//span')[0].text_content()
    except IndexError:
        movie['rating'] = ""
    try:
        movie['storyline'] = hxs.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "summary_text", " " ))]')[
            0].text_content().replace("                    ", "").replace("            ", "")
    except IndexError:
        movie['storyline'] = ""
    try:
        movie['metascore'] = \
            hxs.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "titleReviewBarSubItem", " " ))]//span')[
                0].text_content()
    except IndexError:
        movie['metascore'] = "0"

    return movie
